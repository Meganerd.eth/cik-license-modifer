# +CIK License

# About
The +CIK License is a license modifier that requires the inclusion of the exact phrase "Christ Is King" in the LICENSE file without pre-text followed immediately by newline "\n" or "\r\n"

Christ Is King

# Why?
By including the phrase "Christ Is King" in a LICENSE file that must be distributed with the software you will ensure:

    The software will not be used or hosted by those who reject the King of Kings, Christ our Lord and Savior

# How?
Include the following text in any compatible LICENSE file:
The above copyright notice, this permission notice and the phrase "Christ Is King" shall be included in all copies or substantial portions of the Software.
Example Licenses

    AGPL-3.0-only+CIK
    MIT+CIK

# FAQ
### Do I need to include "Christ Is King" in my code?
No, the inclusion of the LICENSE file is enough. The +CIK modifier has no legal significance for any license that already requires redistribution of the LICENSE file.

### Is +CIK compatible with (A)GPL?
Inclusion of additional legal notices is allowed under §7(b) of the (A)GPL assuming they do not infringe on any of the freedoms granted to the user by the license.
